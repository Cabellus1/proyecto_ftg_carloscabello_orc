﻿' � 2017 ABBYY Production LLC
' SAMPLES code is property of ABBYY, exclusive rights are reserved. 
'
' DEVELOPER is allowed to incorporate SAMPLES into his own APPLICATION and modify it under 
' the  terms of  License Agreement between  ABBYY and DEVELOPER.


' ABBYY FineReader Engine 12 Sample

' Helper subroutines for loading and unloading FREngine.dll

Option Strict Off
Option Explicit On

Imports System.Runtime.InteropServices

Module FREngineLoader

    ' Global FineReader Engine object.
    Public Engine As FREngine.IEngine

    ' This Sub loads and initializes FineReader Engine.
    Public Sub LoadFREngine()
        If Not Engine Is Nothing Then Exit Sub

        ' VB will search for library along the system Path and in
        ' current folder. We change current directory to FineReader
        ' Engine folder to be sure that VB will found FREngine.dll
        ChDrive(Left(VBSamplesConfig.GetFreDllFolder(), 2))
        ChDir(VBSamplesConfig.GetFreDllFolder())

        Dim CustomerProjectId As String
        CustomerProjectId = VBSamplesConfig.GetCustomerProjectId()
        Dim LicensePath As String
        LicensePath = VBSamplesConfig.GetLicensePath()
        Dim LicensePassword As String
        LicensePassword = VBSamplesConfig.GetLicensePassword()

        dllHandle = LoadLibraryEx("FREngine.dll", IntPtr.Zero, LOAD_WITH_ALTERED_SEARCH_PATH)
        Console.WriteLine("CustomerProjectID: " + CustomerProjectId + " LicensePath: " + LicensePath + " LicensePassword: " + LicensePassword)
        Try
            Dim initializeEnginePtr As IntPtr = GetProcAddress(dllHandle, "InitializeEngine")
            If (initializeEnginePtr = IntPtr.Zero) Then
                Throw New System.Exception("Can't find InitializeEngine function")
            End If

            Dim deinitializeEnginePtr As IntPtr = GetProcAddress(dllHandle, "DeinitializeEngine")
            If (deinitializeEnginePtr = IntPtr.Zero) Then
                Throw New System.Exception("Can't find DeinitializeEngine function")
            End If

            Dim dllCanUnloadNowPtr As IntPtr = GetProcAddress(dllHandle, "DllCanUnloadNow")
            If (dllCanUnloadNowPtr = IntPtr.Zero) Then
                Throw New System.Exception("Can't find DllCanUnloadNow function")
            End If

            ' Convert pointers to delegates
            initializeEngineFunc = DirectCast(Marshal.GetDelegateForFunctionPointer(
                initializeEnginePtr, GetType(InitializeEngine)), InitializeEngine)
            deinitializeEngineFunc = DirectCast(Marshal.GetDelegateForFunctionPointer(
                deinitializeEnginePtr, GetType(DeinitializeEngine)), DeinitializeEngine)
            dllCanUnloadNowFunc = DirectCast(Marshal.GetDelegateForFunctionPointer(
                dllCanUnloadNowPtr, GetType(DllCanUnloadNow)), DllCanUnloadNow)

            ' Call the InitializeEngine function
            Dim ret As Integer
            ret = initializeEngineFunc(CustomerProjectId, LicensePath, LicensePassword, "", "", False, Engine)
            Console.WriteLine(ret)
            System.Runtime.InteropServices.Marshal.ThrowExceptionForHR(ret)

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            ' Free the FREngine.dll library
            Engine = Nothing
            ' Deleting all objects before FreeLibrary call
            System.GC.Collect()
            System.GC.WaitForPendingFinalizers()
            System.GC.Collect()
            FreeLibrary(dllHandle)
            dllHandle = IntPtr.Zero
            initializeEngineFunc = Nothing
            deinitializeEngineFunc = Nothing
            dllCanUnloadNowFunc = Nothing

            Throw
        End Try
    End Sub

    ' This Sub deinitializes and unloads FineReader Engine.
    Public Sub UnloadFREngine()
        If Engine Is Nothing Then Exit Sub

        Engine = Nothing
        Dim ret As Integer
        ret = deinitializeEngineFunc()

        ' Deleting all objects before FreeLibrary call
        System.GC.Collect()
        System.GC.WaitForPendingFinalizers()
        System.GC.Collect()
        ret = dllCanUnloadNowFunc()
        If (ret = 0) Then
            FreeLibrary(dllHandle)
        End If

        dllHandle = IntPtr.Zero
        initializeEngineFunc = Nothing
        deinitializeEngineFunc = Nothing
        dllCanUnloadNowFunc = Nothing

        ' thowing exception after cleaning up
        System.Runtime.InteropServices.Marshal.ThrowExceptionForHR(ret)
    End Sub

    ' Kernel32.dll functions
    Private Declare Function LoadLibraryEx Lib "kernel32" Alias "LoadLibraryExA" (
                                ByVal lpLibFileName As String,
                                ByVal hFile As Long,
                                ByVal dwFlags As Long) As IntPtr

    Private Const LOAD_WITH_ALTERED_SEARCH_PATH As Long = &H8
    Private Declare Function GetProcAddress Lib "kernel32" (ByVal hModule As IntPtr, ByVal procedureName As String) As IntPtr
    Private Declare Function FreeLibrary Lib "kernel32" (ByVal hModule As IntPtr) As Boolean

    ' FREngine.dll functions
    <UnmanagedFunctionPointer(CallingConvention.StdCall, CharSet:=CharSet.Unicode)>
    Delegate Function InitializeEngine(ByVal customerProjectId As String, ByVal licensePath As String, ByVal licensePassword As String,
                        ByVal dataFolder As String, ByVal tempFolder As String, ByVal isSharedCPUCoresMode As Boolean,
                        ByRef engineObj As FREngine.IEngine) As Integer

    <UnmanagedFunctionPointer(CallingConvention.StdCall)> Delegate Function DeinitializeEngine() As Integer
    <UnmanagedFunctionPointer(CallingConvention.StdCall)> Delegate Function DllCanUnloadNow() As Integer

    ' Handle to FREngine.dll
    Private dllHandle As IntPtr = IntPtr.Zero

    Private initializeEngineFunc As InitializeEngine = Nothing
    Private deinitializeEngineFunc As DeinitializeEngine = Nothing
    Private dllCanUnloadNowFunc As DllCanUnloadNow = Nothing

End Module
