﻿' © 2017 ABBYY Production LLC
' SAMPLES code is property of ABBYY, exclusive rights are reserved. 
'
' DEVELOPER is allowed to incorporate SAMPLES into his own APPLICATION and modify it under 
' the  terms of  License Agreement between  ABBYY and DEVELOPER.


' Auto-generated config-file for FineReader Engine VB.Net samples

Module VBSamplesConfig

    ' Return full path to folder where FRE dll resides
    Function GetFreDllFolder() As String
        If is64BitConfiguration() Then
            GetFreDllFolder = "C:\Program Files\ABBYY SDK\12\FineReader Engine\Bin64"
        Else
            GetFreDllFolder = "Directory\where\x86\dll\resides"
        End If
    End Function

    ' Return customer project id for FRE
    Function GetCustomerProjectId() As String
        GetCustomerProjectId = "6yjUca7KPXGdX3M7sQ4V"
    End Function

    ' Return path to license file
    Function GetLicensePath() As String
        GetLicensePath = "C:\ProgramData\ABBYY\SDK\12\Licenses\SWET12010006818314857173.ABBYY.ActivationToken"
    End Function

    ' Return license password
    Function GetLicensePassword() As String
        GetLicensePassword = "SWET-1201-0006-8183-1485-7173"
    End Function

    ' Return full path to Samples directory
    Function GetSamplesFolder() As String
        GetSamplesFolder = "C:\ProgramData\ABBYY\SDK\12\FineReader Engine\Samples"
    End Function

    ' Determines whether the current configuration is a 64-bit configuration
    Function is64BitConfiguration() As Boolean
        is64BitConfiguration = System.IntPtr.Size = 8
    End Function

End Module
