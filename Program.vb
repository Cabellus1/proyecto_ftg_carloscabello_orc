Imports System
Imports FREngine
Imports System.IO
Imports System.Net
Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Net.Mail

Module Program
    Public int_amd As Integer
    Public int_BRAUN As Integer
    Public int_fmc As Integer
    Public int_FRESENIUS As Integer
    Public int_HARTMANN As Integer
    Public int_PALEX As Integer
    Public int_TEXPO As Integer
    Public int_noidentificados As Integer
    Private oComando As SqlCommand
    Private oDataReader As SqlDataReader
    Private oConexion As SqlConnection
  

    Sub Main(args As String())
        
        mover_archivos()
        InicializacionEngine()

    End Sub

    'Sub Main(args As String())
    '    '    Console.WriteLine("Hello World!")

    '    proceso1acabado = False
    '    proceso2acabado = False
    '    proceso3acabado = False
    '    proceso4acabado = False
    '    proceso5acabado = False

    '    mover_archivos()
    '    'InicializacionEngine()

    'End Sub
    Private Sub mover_archivos()
        ' Console.WriteLine("hola")
        Try
            Dim ContentFolden() As String = IO.Directory.GetDirectories("C:\Users\informatica.valencia\Desktop\Expediciones\analizados")

            For Each folden As String In ContentFolden
                Console.WriteLine(folden)
                Dim ContentFoldenFolden() As String = IO.Directory.GetFiles(folden)
                For Each file As String In ContentFoldenFolden
                    Dim NameFileSimple As String = file.Substring(file.LastIndexOf("\") + 1)

                    IO.File.Move(file, "C:\Users\informatica.valencia\Desktop\Expediciones\todos\" + NameFileSimple)
                Next

            Next
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        'InicializacionEngine()
    End Sub
    Private Sub InicializacionEngine()
        Try
            ' Load ABBYY FineReader Engine
            Console.WriteLine("Initializing Engine...")
            LoadFREngine()

            ' Load predefined profile for Engine
            Console.WriteLine("Loading predefined profile for Engine...")
            Engine.LoadPredefinedProfile("Default")
            Engine.MultiProcessingParams.MultiProcessingMode = FREngine.MultiProcessingModeEnum.MPM_Parallel
            Engine.MultiProcessingParams.RecognitionProcessesCount = 2
            'Set parent window and caption for Engine's dialog and message boxes
            'Engine.ParentWindow = 64
            'Engine.ApplicationTitle = "Prueba OCR 1"

            AnalizarCarpeta()

        Catch ex As Exception
            Console.WriteLine(ex)

        Finally
            ' Unload Engine
            Console.WriteLine("Deinitializing Engine...")
            UnloadFREngine()

            'Console.WriteLine("")
        End Try

    End Sub
    
    Private Sub AnalizarCarpeta()
     int_amd = 0
        int_BRAUN = 0
        int_fmc = 0
        int_FRESENIUS = 0
        int_HARTMANN = 0
        int_PALEX = 0
        int_TEXPO = 0
        int_noidentificados = 0


        Dim namefolden As String = "C:\Users\informatica.valencia\Desktop\Expediciones\todos"
        Dim ContentFolden() As String = IO.Directory.GetFiles(namefolden)
        AbrirConexion()
        For Each file As String In ContentFolden
            If file.Contains(".pdf") OrElse file.Contains(".jpg") Then
                analizararchivo(file)
            End If
        Next
         CerrarConexion()
       

        If int_noidentificados > 0 Then
           

            Email(int_noidentificados)
        End If
        
    End Sub


  

    

    Private Sub AbrirConexion()
        Try


           Dim servidor As String = "10.10.0.15"
           Dim bd As String = ""
           Dim usuario As String = ""
           Dim pass As String = ""

           'Dim szCadenaConexion = "Provider=SQLOLEDB; " & _"Initial Catalog=" & BaseDatos & "; " & _ "Data Source=" & szgSERVIDOR_SQL_EXPRESS & "; " & _"User ID=web_Gestinmedica2; Password=135798642Wg.;" & _ "Connect Timeout=30;"
           Dim cadena = "data source =" & servidor & "; initial catalog = " & bd & "; user id = " & usuario & "; password = " & pass & ";Connect Timeout=30;"

           ' Dim oConexion As SqlConnection

           '     Console.WriteLine(cadena)

           oConexion = New SqlConnection(cadena)


           oConexion.Open()
           
           Console.WriteLine("Conexion abierta")
        Catch ex As Exception
           Console.WriteLine(ex)
        End Try
    End Sub
    Private Sub CerrarConexion()
        oConexion.Close()
    End Sub
    Private Sub borrarbbdd()
        'Dim consulta As String
        'consulta = "DELETE FROM dbo.Prueba_OCR2 "
        'Dim cmd As New SqlCommand(consulta, oConexion)
        'Dim registro As SqlDataReader = cmd.ExecuteReader
        'registro.Close()

    End Sub
    Private Sub ConsultaExpedicion(cd As String)
        'Dim consulta As String
        'consulta = "select * from dbo.Expediciones where REFERENCIA_EXPEDICION='" & cd & "'"

        'Console.WriteLine(consulta)
        'Dim cmd As New SqlCommand(consulta, oConexion)
        'Dim registro As SqlDataReader = cmd.ExecuteReader
        'Console.WriteLine(registro.Read())
    End Sub

    Private Sub InsertarExpedicion(insertar As String)
      
        insertar = insertar.Replace(vbCrLf, "")
        Dim cmd As New SqlCommand(insertar, oConexion)
        Dim registro As SqlDataReader = cmd.ExecuteReader
        registro.Close()

    End Sub
    Private Sub analizararchivo(archivo As String)
        ' Console.WriteLine("Analizando...")
        Try

            Dim NameFileSimple As String = archivo.Substring(archivo.LastIndexOf("\") + 1)


            Dim FRDocument As FREngine.FRDocument
            FRDocument = Engine.CreateFRDocument
            Dim dpp As DocumentProcessingParams
            dpp = Engine.CreateDocumentProcessingParams()
            dpp.PageProcessingParams.PageAnalysisParams.DetectBarcodes = True
            dpp.PageProcessingParams.PageAnalysisParams.BarcodeParams.Type = BarcodeTypeEnum.BT_Autodetect
            dpp.PageProcessingParams.PageAnalysisParams.BarcodeParams.IsCode39WithoutAsterisk = True
            FRDocument.AddImageFile(archivo)
            FRDocument.Process(dpp)

            Dim texto As String
            texto = LCase(FRDocument.PlainText.Text)

            Dim no_identificado As Boolean
            no_identificado = False

            'AMD
            If texto.Contains("active medical disposable") Then
                '  Console.WriteLine("AMD")
                int_amd += 1
                no_identificado = True
                AMD_Expedicion(FRDocument.Pages(0).Layout, FRDocument, NameFileSimple, archivo)
            End If


            'BRAUN MEDICAL
            If texto.Contains("braun medical") Or texto.Contains("braun") Then
                ' Console.WriteLine("BRAUN")
                int_BRAUN += 1
                no_identificado = True
                BRAUN_Expedicion(FRDocument.Pages(0).Layout, FRDocument, NameFileSimple, archivo)
            End If
            'FMC
            'FRESENIUS MEDICAL CARE


            If texto.Contains("fresenius medical care") Or texto.Contains("a-08834012") Then
                '  Console.WriteLine("FMC")

                int_fmc += 1
                no_identificado = True
                FMC_Expedicion(FRDocument.Pages(0).Layout, FRDocument, NameFileSimple, archivo)
            End If
            'FRESENIUS KABI


            If texto.Contains("fresenius kabi") Then
                '    Console.WriteLine("FRESENIUS KABI")

                int_FRESENIUS += 1
                no_identificado = True
                FRESENIUS_Expedicion(FRDocument.Pages(0).Layout, FRDocument, NameFileSimple, archivo)
            End If

            'HARTMANN


            If texto.Contains("hartmann") Then
                ' Console.WriteLine("HARTMANN")

                int_HARTMANN += 1
                no_identificado = True
                HARTMANN_Expedicion(FRDocument.Pages(0).Layout, FRDocument, NameFileSimple, archivo)
            End If

            'PALEX


            If texto.Contains("palex") Then
                '   Console.WriteLine("PALEX")

                int_PALEX += 1
                no_identificado = True
                PALEX_Expedicion(FRDocument.Pages(0).Layout, FRDocument, NameFileSimple, archivo)
            End If

            'TEXPOL


            If texto.Contains("texpol") Then
                '  Console.WriteLine("TEXPOL")

                int_TEXPO += 1
                no_identificado = True
                TEXPOL_Expedicion(FRDocument.Pages(0).Layout, FRDocument, NameFileSimple, archivo)
            End If

            If no_identificado = False Then
                '  Console.WriteLine("NO IDENTIFICADO")
                int_noidentificados += 1
                Expedicion_NoIdentificado(FRDocument, NameFileSimple, archivo)
            End If

            

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        '   Console.WriteLine("Terminado...")

    End Sub

    Private Sub AMD_Expedicion(expedicion As ILayout, archivo As FRDocument, name_file As String, file As String)

        'C:\Users\informatica.valencia\Desktop\Expediciones\analizados\AMD
        Dim texto_archivo As String
        Dim fecha As String
        Dim n_pedido As String
        Dim total As String

        Dim region_Albaran As IRegion
        region_Albaran = Engine.CreateRegion
        region_Albaran.AddRect(400, 150, 600, 190)

        Dim region_Albaran2 As IRegion
        region_Albaran2 = Engine.CreateRegion
        region_Albaran2.AddRect(726, 150, 1000, 261)


        Dim region_fecha As IRegion
        region_fecha = Engine.CreateRegion
        region_fecha.AddRect(300, 145, 409, 169)


        Dim region_n_pedido As IRegion
        region_n_pedido = Engine.CreateRegion
        region_n_pedido.AddRect(375, 247, 623, 286)

        Dim region_total As IRegion
        region_total = Engine.CreateRegion
        region_total.AddRect(1389, 1919, 1552, 1969)

        Dim block_albaran As IBlock
        Dim block_albaran2 As IBlock

        Dim block_Fecha As IBlock
        Dim block_n_pedido As IBlock
        Dim block_total As IBlock


        block_albaran = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_Albaran, expedicion.Blocks.Count)
        block_albaran2 = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_Albaran2, expedicion.Blocks.Count)

        block_Fecha = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_fecha, expedicion.Blocks.Count)
        block_n_pedido = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_n_pedido, expedicion.Blocks.Count)
        block_total = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_total, expedicion.Blocks.Count)
        archivo.Recognize()

        Dim albaran As String
        Dim boolean_albaran As Boolean
        boolean_albaran = False
        For Each para As IParagraph In block_albaran.GetAsTextBlock.Text.Paragraphs
            albaran = para.Text
            albaran = albaran.Replace("*", "")
            albaran = albaran.Replace(" ", "")

            Console.WriteLine(albaran.Count)

            If albaran.Count = 1 Then
                boolean_albaran = True

            Else
                texto_archivo &= "ALBARAN: " + para.Text + " " + vbCrLf

            End If


        Next
        'Console.WriteLine(boolean_albaran)
        If boolean_albaran Then
            For Each para As IParagraph In block_albaran2.GetAsTextBlock.Text.Paragraphs
                albaran = para.Text

                texto_archivo &= "ALBARAN: " + para.Text + " " + vbCrLf

            Next
        End If

        For Each para As IParagraph In block_Fecha.GetAsTextBlock.Text.Paragraphs
            fecha = para.Text
            

        Next
        For Each para As IParagraph In block_n_pedido.GetAsTextBlock.Text.Paragraphs
            n_pedido = para.Text
            
        Next
        For Each para As IParagraph In block_total.GetAsTextBlock.Text.Paragraphs
            total = para.Text
           
        Next

        'IO.File.Create("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\AMD\" + albaran + ".txt")
        'Dim objfile As New System.IO.StreamWriter("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\AMD\" + albaran + ".txt")

        'objfile.Write(texto_archivo)
        ' ConsultaExpedicion(albaran)
        'Console.WriteLine(texto_archivo)
        archivo.Close()
        Dim insert As String
        fecha = fecha.Replace(vbCrLf, "")
        n_pedido = n_pedido.Replace(vbCrLf, "")
        total = total.Replace(vbCrLf, "")
        fecha = fecha.Trim(" ")
        n_pedido = n_pedido.Trim(" ")
        total = total.Trim(" ")

        'If total = "" Then
        '    total = Null
        'End If

        insert = "INSERT INTO dbo.Prueba_OCR2 (CB, FECHA, EMPRESA, NUM_REFERENCIA, NUM_CLIENTE, NUM_DOCUMENTACION, NUM_AGRUPACION, NIF, TOTAL) VALUES ('" & albaran & "', NULL, 'AMD', NULL, NULL, NULL, NULL, NULL, '" & total & "') "
        InsertarExpedicion(insert)
        ' Console.WriteLine(texto_archivo)

        IO.File.Move(file, "C:\Users\informatica.valencia\Desktop\Expediciones\analizados\AMD\" + name_file) 'ENVIA ARCHIVO NO IDENTIFICADO A LA CARPETA DE DESTINO NO IDENTIFICADO
        ' Create_file("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\AMD\" & albaran & ".txt", texto_archivo)


    End Sub
    Private Sub BRAUN_Expedicion(expedicion As ILayout, archivo As FRDocument, name_file As String, file As String)

        'C:\Users\informatica.valencia\Desktop\Expediciones\analizados\BRAUN
        Dim texto_archivo As String

        Dim cb As String

        Dim region_Albaran As IRegion
        region_Albaran = Engine.CreateRegion
        region_Albaran.AddRect(1349, 2212, 1493, 2252)

        Dim fecha As String

        Dim region_fecha As IRegion
        region_fecha = Engine.CreateRegion
        region_fecha.AddRect(1078, 2200, 1194, 2245)

        Dim block_albaran As IBlock
        Dim block_Fecha As IBlock

        block_albaran = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_Albaran, expedicion.Blocks.Count)
        block_Fecha = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_fecha, expedicion.Blocks.Count)
        For Each bloque As IBlock In archivo.Pages(0).Layout.Blocks
            If bloque.Type = FREngine.BlockTypeEnum.BT_Barcode Then
                cb = bloque.GetAsBarcodeBlock.Text
                cb = cb.Replace("*", "")
                cb = cb.Trim(" ")

                texto_archivo &= "CODIGO DE BARRAS: " + cb + " " + vbCrLf

            End If
        Next
        archivo.Recognize()
        For Each para As IParagraph In block_albaran.GetAsTextBlock.Text.Paragraphs

            texto_archivo &= "ALBARAN: " + para.Text + " " + vbCrLf

        Next

        For Each para As IParagraph In block_Fecha.GetAsTextBlock.Text.Paragraphs
            fecha = para.Text
            texto_archivo &= "FECHA: " + para.Text + " " + vbCrLf

        Next
        'IO.File.Create("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\BRAUN\" + cb + ".txt")
        'Dim objfile As New System.IO.StreamWriter("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\BRAUN\" + cb + ".txt")

        'objfile.Write(texto_archivo)
        'Console.WriteLine(texto_archivo)

        archivo.Close()
        Dim insert As String
        fecha = fecha.Replace(vbCrLf, "")
        fecha = fecha.Replace(" ", "")

        fecha = fecha.Trim(" ")
        fecha = fecha.Replace(".", "/")

        insert = "INSERT INTO dbo.Prueba_OCR2 (CB, FECHA, EMPRESA, NUM_REFERENCIA, NUM_CLIENTE, NUM_DOCUMENTACION, NUM_AGRUPACION, NIF, TOTAL) VALUES ('" & cb & "', '" & fecha & "', 'BRAUN', NULL, NULL, NULL, NULL, NULL,NULL) "
        InsertarExpedicion(insert)

        ' ConsultaExpedicion(cb)
        '  Console.WriteLine(texto_archivo)

        'Create_file("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\BRAUN\" & cb & ".txt", texto_archivo)

        IO.File.Move(file, "C:\Users\informatica.valencia\Desktop\Expediciones\analizados\BRAUN\" + name_file) 'ENVIA ARCHIVO NO IDENTIFICADO A LA CARPETA DE DESTINO NO IDENTIFICADO


    End Sub
    Private Sub FMC_Expedicion(expedicion As ILayout, archivo As FRDocument, name_file As String, file As String)


        Dim texto_archivo As String

        Dim cb As String
        Dim fecha As String
        Dim Referencia As String
        Dim Cliente As String
        Dim n_documento As String
        Dim NIF As String
        Dim total As String


        'C:\Users\informatica.valencia\Desktop\Expediciones\analizados\FMC

        Dim region_fecha As IRegion
        region_fecha = Engine.CreateRegion
        'region_fecha.AddRect(1994, 667, 2157, 717)
        region_fecha.AddRect(1970, 671, 2164, 726)


        Dim region_referencia As IRegion
        region_referencia = Engine.CreateRegion
        region_referencia.AddRect(1717, 614, 1970, 717)

        Dim region_n_documento As IRegion
        region_n_documento = Engine.CreateRegion
        region_n_documento.AddRect(1505, 614, 1704, 717)

        Dim region_cliente As IRegion
        region_cliente = Engine.CreateRegion
        region_cliente.AddRect(45, 687, 200, 730)

        Dim region_nif As IRegion
        region_nif = Engine.CreateRegion
        region_nif.AddRect(1850, 550, 2030, 600)

        Dim region_total As IRegion
        region_total = Engine.CreateRegion
        region_total.AddRect(1920, 1448, 2078, 1494)


        Dim block_Fecha As IBlock
        Dim block_referencia As IBlock
        Dim block_n_documento As IBlock
        Dim block_cliente As IBlock
        Dim block_nif As IBlock
        Dim block_total As IBlock

        block_Fecha = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_fecha, expedicion.Blocks.Count)
        block_referencia = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_referencia, expedicion.Blocks.Count)
        block_n_documento = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_n_documento, expedicion.Blocks.Count)
        block_cliente = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_cliente, expedicion.Blocks.Count)
        block_nif = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_nif, expedicion.Blocks.Count)
        block_total = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_total, expedicion.Blocks.Count)

        For Each bloque As IBlock In archivo.Pages(0).Layout.Blocks
            If bloque.Type = FREngine.BlockTypeEnum.BT_Barcode Then
                cb = bloque.GetAsBarcodeBlock.Text
                cb = cb.Replace("*", "")
                cb = cb.Replace(" ", "")

                texto_archivo &= "CODIGO DE BARRAS: " + cb + " " + vbCrLf

            End If
        Next

        archivo.Recognize()

        Dim count_para_fech As Integer
        count_para_fech = 0
        For Each para As IParagraph In block_Fecha.GetAsTextBlock.Text.Paragraphs

            If block_Fecha.GetAsTextBlock.Text.Paragraphs.Count = 2 Then

                If count_para_fech = 1 Then
                    fecha = para.Text
                    texto_archivo &= "FECHA: " + para.Text + " " + vbCrLf
                End If
                count_para_fech += 1
            Else
                fecha = para.Text

                texto_archivo &= "FECHA: " + para.Text + " " + vbCrLf

            End If


        Next
        For Each para As IParagraph In block_referencia.GetAsTextBlock.Text.Paragraphs
            Referencia = para.Text
            texto_archivo &= "REFERENCIA: " + para.Text + " " + vbCrLf

        Next
        For Each para As IParagraph In block_n_documento.GetAsTextBlock.Text.Paragraphs
            n_documento = para.Text
            texto_archivo &= "N�DOCUMENTO: " + para.Text + " " + vbCrLf

        Next
        For Each para As IParagraph In block_cliente.GetAsTextBlock.Text.Paragraphs
            Cliente = para.Text
            texto_archivo &= "CLIENTE: " + para.Text + " " + vbCrLf

        Next
        For Each para As IParagraph In block_nif.GetAsTextBlock.Text.Paragraphs
            NIF = para.Text
            texto_archivo &= "N.F.I: " + para.Text + " " + vbCrLf

        Next
        For Each para As IParagraph In block_total.GetAsTextBlock.Text.Paragraphs
            total = para.Text
            texto_archivo &= "TOTAL: " + para.Text + " " + vbCrLf

        Next
        'IO.File.Create("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\FMC\" + cb + ".txt")
        'Dim objfile As New System.IO.StreamWriter("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\FMC\" + cb + ".txt")

        'objfile.Write(texto_archivo)
        'Console.WriteLine(texto_archivo)
        archivo.Close()
        ' ConsultaExpedicion(cb)
        ' Console.WriteLine(texto_archivo)
        Dim INSERT As String

        fecha = fecha.Replace(vbCrLf, "")
        Referencia = Referencia.Replace(vbCrLf, "")
        n_documento = n_documento.Replace(vbCrLf, "")
        Cliente = Cliente.Replace(vbCrLf, "")
        NIF = NIF.Replace(vbCrLf, "")
        total = total.Replace(vbCrLf, "")

        fecha = fecha.Replace(" ", "")
        Referencia = Referencia.Replace(" ", "")
        n_documento = n_documento.Replace(" ", "")
        Cliente = Cliente.Replace(" ", "")
        NIF = NIF.Replace(" ", "")
        total = total.Replace(" ", "")

        fecha = fecha.Trim(" ")
        Referencia = Referencia.Trim(" ")
        n_documento = n_documento.Trim(" ")
        Cliente = Cliente.Trim(" ")
        NIF = NIF.Trim(" ")
        total = total.Trim(" ")



        INSERT = "INSERT INTO dbo.Prueba_OCR2 (CB, FECHA, EMPRESA, NUM_REFERENCIA, NUM_CLIENTE, NUM_DOCUMENTACION, NUM_AGRUPACION, NIF, TOTAL) VALUES ('" & cb & "', '" & fecha & "', 'FMC', NULL, '" & Cliente & "', '" & n_documento & "', NULL, '" & NIF & "','" & total & "') "
        InsertarExpedicion(INSERT)

        ' Create_file("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\FMC\" & cb & ".txt", texto_archivo)

        IO.File.Move(file, "C:\Users\informatica.valencia\Desktop\Expediciones\analizados\FMC\" + name_file) 'ENVIA ARCHIVO NO IDENTIFICADO A LA CARPETA DE DESTINO NO IDENTIFICADO


    End Sub
    Private Sub FRESENIUS_Expedicion(expedicion As ILayout, archivo As FRDocument, name_file As String, file As String)

        'Console.WriteLine("WIDTH = " & expedicion.Width)

        Dim mayor As Boolean
        mayor = False

        If expedicion.Width > 2000 Then
            ' Console.WriteLine("Mayor de 2000")
            mayor = True
        Else
            '  Console.WriteLine("Menor de 2000")
            mayor = False

        End If

        Dim texto_archivo As String


        Dim cb As String
        'C:\Users\informatica.valencia\Desktop\Expediciones\analizados\FRESENIUS KABI

        Dim region_n_agrupacion As IRegion
        region_n_agrupacion = Engine.CreateRegion
        'region_n_agrupacion.AddRect(976, 885, 1115, 926)
        'region_n_agrupacion.AddRect(1432, 1330, 1773, 1397)



        Dim region_n_documento As IRegion
        region_n_documento = Engine.CreateRegion
        'region_n_documento.AddRect(1225, 883, 1389, 921)
        'region_n_documento.AddRect(1764, 1331, 2086, 1393)

        Dim region_fecha As IRegion
        region_fecha = Engine.CreateRegion
        'region_fecha.AddRect(1393, 880, 1540, 918)
        'region_fecha.AddRect(2073, 1320, 1329, 1392)


        Dim region_cliente As IRegion
        region_cliente = Engine.CreateRegion
        'region_cliente.AddRect(46, 971, 197, 1000)
        'region_cliente.AddRect(14, 1457, 316, 1519)

        Dim region_referencia As IRegion
        region_referencia = Engine.CreateRegion
        'region_referencia.AddRect(202, 975, 350, 1000)

        If mayor Then
            region_n_agrupacion.AddRect(1466, 1328, 1716, 1398)
            region_n_documento.AddRect(1837, 1323, 2087, 1397)
            region_fecha.AddRect(2086, 1321, 2318, 1387)
            region_cliente.AddRect(68, 1460, 294, 1518)
            region_referencia.AddRect(295, 1460, 719, 1516)
        Else
            region_n_agrupacion.AddRect(975, 882, 1171, 926)
            region_n_documento.AddRect(1223, 879, 1390, 924)
            region_fecha.AddRect(1392, 877, 1543, 919)
            region_cliente.AddRect(43, 969, 196, 1011)
            region_referencia.AddRect(195, 970, 481, 1007)

        End If

        Dim n_agrupacion As String
        Dim n_documentacion As String
        Dim fecha As String
        Dim cliente As String
        Dim referencia As String

        Dim block_n_agrupacion As IBlock
        Dim block_n_documento As IBlock
        Dim block_Fecha As IBlock
        Dim block_cliente As IBlock
        Dim block_referencia As IBlock

        block_n_agrupacion = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_n_agrupacion, expedicion.Blocks.Count)
        block_n_documento = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_n_documento, expedicion.Blocks.Count)
        block_Fecha = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_fecha, expedicion.Blocks.Count)
        block_cliente = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_cliente, expedicion.Blocks.Count)
        block_referencia = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_referencia, expedicion.Blocks.Count)

        For Each bloque As IBlock In archivo.Pages(0).Layout.Blocks
            If bloque.Type = FREngine.BlockTypeEnum.BT_Barcode Then
                cb = bloque.GetAsBarcodeBlock.Text
                cb = cb.Replace("*", "")
                cb = cb.Replace(" ", "")

                texto_archivo &= "CODIGO DE BARRAS: " + cb + " " + vbCrLf

            End If

        Next
        archivo.Recognize()
        For Each para As IParagraph In block_n_agrupacion.GetAsTextBlock.Text.Paragraphs
            n_agrupacion = para.Text
            texto_archivo &= "N�AGRUPACION: " + para.Text + " " + vbCrLf

        Next
        For Each para As IParagraph In block_n_documento.GetAsTextBlock.Text.Paragraphs
            n_documentacion = para.Text
            texto_archivo &= "N�DOCUMENTO: " + para.Text + " " + vbCrLf

        Next
        For Each para As IParagraph In block_Fecha.GetAsTextBlock.Text.Paragraphs
            fecha = para.Text
            texto_archivo &= "FECHA: " + para.Text + " " + vbCrLf

        Next
        For Each para As IParagraph In block_cliente.GetAsTextBlock.Text.Paragraphs
            cliente = para.Text
            texto_archivo &= "CLIENTE: " + para.Text + " " + vbCrLf

        Next
        For Each para As IParagraph In block_referencia.GetAsTextBlock.Text.Paragraphs
            referencia = para.Text
            texto_archivo &= "REFERENCIA: " + para.Text + " " + vbCrLf

        Next
        'IO.File.Create("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\FRESENIUS KABI\" + cb + ".txt")
        'Dim objfile As New System.IO.StreamWriter("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\FRESENIUS KABI\" + cb + ".txt")

        'objfile.Write(texto_archivo)
        'Console.WriteLine(texto_archivo)
        archivo.Close()
        ' Console.WriteLine(texto_archivo)
        Dim INSERT As String

        fecha = fecha.Replace(vbCrLf, "")
        referencia = referencia.Replace(vbCrLf, "")
        cliente = cliente.Replace(vbCrLf, "")
        n_documentacion = n_documentacion.Replace(vbCrLf, "")
        n_agrupacion = n_agrupacion.Replace(vbCrLf, "")

        fecha = fecha.Replace(" ", "")
        referencia = referencia.Replace(" ", "")
        cliente = cliente.Replace(" ", "")
        n_documentacion = n_documentacion.Replace(" ", "")
        n_agrupacion = n_agrupacion.Replace(" ", "")
        n_agrupacion = n_agrupacion.Replace(" ", "")
        n_agrupacion = n_agrupacion.Replace(" ", "")

        fecha = fecha.Trim(" ")
        referencia = referencia.Trim(" ")
        cliente = cliente.Trim(" ")
        n_documentacion = n_documentacion.Trim(" ")
        n_agrupacion = n_agrupacion.Trim(" ")



        INSERT = "INSERT INTO dbo.Prueba_OCR2 (CB, FECHA, EMPRESA, NUM_REFERENCIA, NUM_CLIENTE, NUM_DOCUMENTACION, NUM_AGRUPACION, NIF, TOTAL) VALUES ('" & cb & "', '" & fecha & "', 'FRESENIUS', NULL, '" & cliente & "', '" & n_documentacion & "', '" & n_agrupacion & "', NULL,NULL) "
        InsertarExpedicion(INSERT)

        ' ConsultaExpedicion(cb)
        'Create_file("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\FRESENIUS KABI\" & cb & ".txt", texto_archivo)

        IO.File.Move(file, "C:\Users\informatica.valencia\Desktop\Expediciones\analizados\FRESENIUS KABI\" + name_file) 'ENVIA ARCHIVO NO IDENTIFICADO A LA CARPETA DE DESTINO NO IDENTIFICADO


    End Sub
    Private Sub HARTMANN_Expedicion(expedicion As ILayout, archivo As FRDocument, name_file As String, file As String)

        'C:\Users\informatica.valencia\Desktop\Expediciones\analizados\HARTMANN
        Dim texto_archivo As String

        Dim cb As String
        Dim fecha As String
        Dim cliente As String


        Dim region_fecha As IRegion
        region_fecha = Engine.CreateRegion
        region_fecha.AddRect(1808, 280, 1955, 313)

        Dim region_cliente As IRegion
        region_cliente = Engine.CreateRegion
        region_cliente.AddRect(1808, 310, 1985, 346)

        Dim block_Fecha As IBlock
        Dim block_cliente As IBlock

        block_Fecha = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_fecha, expedicion.Blocks.Count)
        block_cliente = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_cliente, expedicion.Blocks.Count)

        For Each bloque As IBlock In archivo.Pages(0).Layout.Blocks
            If bloque.Type = FREngine.BlockTypeEnum.BT_Barcode Then
                cb = bloque.GetAsBarcodeBlock.Text
                cb = cb.Replace("*", "")
                cb = cb.Trim(" ")

                texto_archivo &= "CODIGO DE BARRAS: " + cb + " " + vbCrLf

            End If
        Next


        archivo.Recognize()
        For Each para As IParagraph In block_Fecha.GetAsTextBlock.Text.Paragraphs
            fecha = para.Text
            texto_archivo &= "FECHA: " + para.Text + " " + vbCrLf

        Next
        For Each para As IParagraph In block_cliente.GetAsTextBlock.Text.Paragraphs
            cliente = para.Text
            texto_archivo &= "CLIENTE: " + para.Text + " " + vbCrLf

        Next
        'IO.File.Create("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\HARTMANN\" + cb + ".txt")
        'Dim objfile As New System.IO.StreamWriter("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\HARTMANN\" + cb + ".txt")

        'objfile.Write(texto_archivo)
        'Console.WriteLine(texto_archivo)
        archivo.Close()
        ' ConsultaExpedicion(cb)
        ' Console.WriteLine(texto_archivo)
        Dim INSERT As String

        fecha = fecha.Replace(vbCrLf, "")

        cliente = cliente.Replace(vbCrLf, "")

        fecha = fecha.Replace(" ", "")

        cliente = cliente.Replace(" ", "")

        fecha = fecha.Trim(" ")

        cliente = cliente.Trim(" ")


        INSERT = "INSERT INTO dbo.Prueba_OCR2 (CB, FECHA, EMPRESA, NUM_REFERENCIA, NUM_CLIENTE, NUM_DOCUMENTACION, NUM_AGRUPACION, NIF, TOTAL) VALUES ('" & cb & "', '" & fecha & "', 'HARTMANN', null, '" & cliente & "', NULL, NULL, NULL,NULL) "
        InsertarExpedicion(INSERT)

        'Create_file("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\HARTMANN\" & cb & ".txt", texto_archivo)

        IO.File.Move(file, "C:\Users\informatica.valencia\Desktop\Expediciones\analizados\HARTMANN\" + name_file) 'ENVIA ARCHIVO NO IDENTIFICADO A LA CARPETA DE DESTINO NO IDENTIFICADO

    End Sub
    Private Sub PALEX_Expedicion(expedicion As ILayout, archivo As FRDocument, name_file As String, file As String)
        'C:\Users\informatica.valencia\Desktop\Expediciones\analizados\PALEX

        Dim texto_archivo As String

        Dim cb As String

        Dim num_barcode As Integer
        num_barcode = 0
        For Each bloque As IBlock In archivo.Pages(0).Layout.Blocks
            If bloque.Type = FREngine.BlockTypeEnum.BT_Barcode Then

                If num_barcode < bloque.GetAsBarcodeBlock.Text.Count Then
                    num_barcode = bloque.GetAsBarcodeBlock.Text.Count
                    cb = bloque.GetAsBarcodeBlock.Text

                End If

            End If
        Next
        cb = cb.Replace("*", "")
        cb = cb.Replace(" ", "")

        texto_archivo &= "CODIGO DE BARRAS: " + cb + " " + vbCrLf
        Dim region_cb As IRegion
        region_cb = Engine.CreateRegion
        region_cb.AddRect(452, 1434, 748, 1501)

        Dim region_expedicion As IRegion
        region_expedicion = Engine.CreateRegion
        region_expedicion.AddRect(922, 1542, 1134, 1581)

        Dim region_fecha As IRegion
        region_fecha = Engine.CreateRegion
        'region_fecha.AddRect(1680, 1543, 1831, 1579)
        region_fecha.AddRect(1646, 1534, 1845, 1581)

        Dim region_cliente As IRegion
        region_cliente = Engine.CreateRegion
        'region_cliente.AddRect(1834, 1543, 1970, 1579)
        region_cliente.AddRect(1844, 1530, 1977, 1580)

        Dim region_nif As IRegion
        region_nif = Engine.CreateRegion
        region_nif.AddRect(1228, 139, 1438, 178)

        Dim region_total As IRegion
        region_total = Engine.CreateRegion
        'region_total.AddRect(2122, 1374, 2228, 1413)
        region_total.AddRect(2045, 1352, 2239, 1417)

        Dim block_cb As IBlock
        Dim block_expedicion As IBlock
        Dim block_Fecha As IBlock
        Dim block_cliente As IBlock
        Dim block_total As IBlock

        Dim n_expedicion As String
        Dim fecha As String
        Dim cliente As String
        Dim total As String

        block_cb = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_cb, expedicion.Blocks.Count)
        block_expedicion = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_expedicion, expedicion.Blocks.Count)
        block_Fecha = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_fecha, expedicion.Blocks.Count)
        block_cliente = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_cliente, expedicion.Blocks.Count)
        block_total = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_total, expedicion.Blocks.Count)
        archivo.Recognize()
        'For Each para As IParagraph In block_cb.GetAsTextBlock.Text.Paragraphs
        '    cb = para.Text
        '    texto_archivo &= "CODIGO DE BARRAS: " + para.Text + " " + vbCrLf

        'Next

        For Each para As IParagraph In block_expedicion.GetAsTextBlock.Text.Paragraphs
            n_expedicion = para.Text
            texto_archivo &= "EXPEDICION: " + para.Text + " " + vbCrLf

        Next
        'Console.WriteLine(block_Fecha.GetAsTextBlock.Text.Paragraphs.Count)

        Dim count_para_fech As Integer
        count_para_fech = 0
        For Each para As IParagraph In block_Fecha.GetAsTextBlock.Text.Paragraphs

            If block_Fecha.GetAsTextBlock.Text.Paragraphs.Count = 2 Then
                If count_para_fech = 1 Then
                    fecha = para.Text
                    texto_archivo &= "FECHA: " + para.Text + " " + vbCrLf
                End If

                count_para_fech += 1
            Else
                fecha = para.Text

                texto_archivo &= "FECHA: " + para.Text + " " + vbCrLf
            End If



        Next
        Dim count_para_client As Integer
        count_para_client = 0
        For Each para As IParagraph In block_cliente.GetAsTextBlock.Text.Paragraphs

            If block_cliente.GetAsTextBlock.Text.Paragraphs.Count = 2 Then
                If count_para_client = 1 Then
                    cliente = para.Text
                    texto_archivo &= "CLIENTE: " + para.Text + " " + vbCrLf
                End If

                count_para_client += 1
            Else
                cliente = para.Text

                texto_archivo &= "CLIENTE: " + para.Text + " " + vbCrLf
            End If



        Next
        For Each para As IParagraph In block_total.GetAsTextBlock.Text.Paragraphs
            total = para.Text
            texto_archivo &= "TOTAL: " + para.Text + " " + vbCrLf

        Next

        'IO.File.Create("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\PALEX\" + cb + ".txt")
        'Dim objfile As New System.IO.StreamWriter("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\PALEX\" + cb + ".txt")

        'objfile.Write(texto_archivo)

        'Console.WriteLine(texto_archivo)
        archivo.Close()
        'ConsultaExpedicion(cb)
        'Console.WriteLine(texto_archivo)

        fecha = fecha.Replace(vbCrLf, "")
        n_expedicion = n_expedicion.Replace(vbCrLf, "")
        cliente = cliente.Replace(vbCrLf, "")
        total = total.Replace(vbCrLf, "")

        fecha = fecha.Replace(" ", "")
        n_expedicion = n_expedicion.Replace(" ", "")
        cliente = cliente.Replace(" ", "")
        total = total.Replace(" ", "")

        fecha = fecha.Trim(" ")
        n_expedicion = n_expedicion.Trim(" ")
        cliente = cliente.Trim(" ")
        total = total.Trim(" ")

        fecha = fecha.Replace(".", "/")

        Dim INSERT As String
        INSERT = "INSERT INTO dbo.Prueba_OCR2 (CB, FECHA, EMPRESA, NUM_REFERENCIA, NUM_CLIENTE, NUM_DOCUMENTACION, NUM_AGRUPACION, NIF, TOTAL) VALUES ('" & cb & "', '" & fecha & "', 'PALEX', '" & n_expedicion & "', '" & cliente & "', NULL, NULL, NULL,'" & total & "') "
        InsertarExpedicion(INSERT)

        'Create_file("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\PALEX\" & cb & ".txt", texto_archivo)

        IO.File.Move(file, "C:\Users\informatica.valencia\Desktop\Expediciones\analizados\PALEX\" + name_file) 'ENVIA ARCHIVO NO IDENTIFICADO A LA CARPETA DE DESTINO NO IDENTIFICADO


    End Sub
    Private Sub TEXPOL_Expedicion(expedicion As ILayout, archivo As FRDocument, name_file As String, file As String)
        'C:\Users\informatica.valencia\Desktop\Expediciones\analizados\TEXPOL

        Dim cb As String

        Dim texto_archivo As String

        Dim fecha As String
        Dim n_expedicion As String
        Dim total As String

        Dim region_Albaran As IRegion
        region_Albaran = Engine.CreateRegion
        region_Albaran.AddRect(96, 411, 378, 450)

        Dim region_fecha As IRegion
        region_fecha = Engine.CreateRegion
        region_fecha.AddRect(380, 410, 605, 450)

        Dim region_expediente As IRegion
        region_expediente = Engine.CreateRegion
        region_expediente.AddRect(100, 500, 820, 545)

        Dim region_total As IRegion
        region_total = Engine.CreateRegion
        region_total.AddRect(1234, 2060, 1517, 2101)

        Dim block_albaran As IBlock
        Dim block_Fecha As IBlock
        Dim block_expediente As IBlock
        Dim block_total As IBlock

        block_albaran = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_Albaran, expedicion.Blocks.Count)
        block_Fecha = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_fecha, expedicion.Blocks.Count)
        block_expediente = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_expediente, expedicion.Blocks.Count)
        block_total = expedicion.Blocks.AddNew(FREngine.BlockTypeEnum.BT_Text, region_total, expedicion.Blocks.Count)

        For Each bloque As IBlock In archivo.Pages(0).Layout.Blocks
            If bloque.Type = FREngine.BlockTypeEnum.BT_Barcode Then
                cb = bloque.GetAsBarcodeBlock.Text
                cb = cb.Replace("*", "")
                cb = cb.Replace(" ", "")

                texto_archivo &= "CODIGO DE BARRAS: " + cb + " " + vbCrLf
            End If
        Next

        archivo.Recognize()

        For Each para As IParagraph In block_albaran.GetAsTextBlock.Text.Paragraphs

            texto_archivo &= "ALBARAN: " + para.Text + " " + vbCrLf

        Next
        For Each para As IParagraph In block_Fecha.GetAsTextBlock.Text.Paragraphs
            fecha = para.Text
            texto_archivo &= "FECHA: " + para.Text + " " + vbCrLf

        Next
        For Each para As IParagraph In block_expediente.GetAsTextBlock.Text.Paragraphs
            n_expedicion = para.Text
            texto_archivo &= "EXPEDIENTE: " + para.Text + " " + vbCrLf

        Next
        For Each para As IParagraph In block_total.GetAsTextBlock.Text.Paragraphs
            total = para.Text
            texto_archivo &= "TOTAL: " + para.Text + " " + vbCrLf

        Next

        'IO.File.Create("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\TEXPOL\" + cb + ".txt")
        'Dim objfile As New System.IO.StreamWriter("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\TEXPOL\" + cb + ".txt")

        'objfile.Write(texto_archivo)

        Console.WriteLine(texto_archivo)
        archivo.Close()
        ' ConsultaExpedicion(cb)

        fecha = fecha.Replace(vbCrLf, "")
        n_expedicion = n_expedicion.Replace(vbCrLf, "")
        total = total.Replace(vbCrLf, "")

        fecha = fecha.Replace(" ", "")
        n_expedicion = n_expedicion.Replace(" ", "")
        total = total.Replace(" ", "")

        fecha = fecha.Trim(" ")
        n_expedicion = n_expedicion.Trim(" ")
        total = total.Trim(" ")


        Dim INSERT As String





        INSERT = "INSERT INTO dbo.Prueba_OCR2 (CB, FECHA, EMPRESA, NUM_REFERENCIA, NUM_CLIENTE, NUM_DOCUMENTACION, NUM_AGRUPACION, NIF, TOTAL) VALUES ('" & cb & "', '" & fecha & "', 'TEXPOL', NULL, null, NULL, NULL, NULL,'" & total & "') "

        InsertarExpedicion(INSERT)

        'Create_file("C:\Users\informatica.valencia\Desktop\Expediciones\analizados\TEXPOL\" & cb & ".txt", texto_archivo)
        IO.File.Move(file, "C:\Users\informatica.valencia\Desktop\Expediciones\analizados\TEXPOL\" + name_file) 'ENVIA ARCHIVO NO IDENTIFICADO A LA CARPETA DE DESTINO NO IDENTIFICADO


    End Sub

    Private Sub Expedicion_NoIdentificado(archivo As FRDocument, name_file As String, file As String)
        'C:\Users\informatica.valencia\Desktop\Expediciones\analizados\NO IDENTIFICADOS

        Dim cb As String
        For Each bloque As IBlock In archivo.Pages(0).Layout.Blocks
            If bloque.Type = FREngine.BlockTypeEnum.BT_Barcode Then
                cb = bloque.GetAsBarcodeBlock.Text
                cb = cb.Replace("*", "")
                cb = cb.Replace(" ", "")

            End If
        Next

        archivo.Close()
        'ConsultaExpedicion(cb)

        IO.File.Move(file, "C:\Users\informatica.valencia\Desktop\Expediciones\analizados\NO IDENTIFICADOS\" + name_file) 'ENVIA ARCHIVO NO IDENTIFICADO A LA CARPETA DE DESTINO NO IDENTIFICADO

    End Sub

    Private Sub Create_file(ruta_archivo As String, contenido As String)

        Console.WriteLine(ruta_archivo)

        IO.File.Create(ruta_archivo).Dispose()

        Try
            Dim archivo As System.IO.StreamWriter
            archivo = File.AppendText(ruta_archivo)

            archivo.Write(contenido)
            archivo.Close()
        Catch ex As Exception

            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Private Sub Email(no_identificado As Integer)
        Dim Contenido As String

        Contenido = "Tienes " & no_identificado & " archivos que no se han podido identificar"

        Dim message As New MailMessage
        Dim smtp As New SmtpClient
      
        With message
            .From = New System.Net.Mail.MailAddress("pruebagestinmedica@gmail.com")
            .To.Add("carlitoscabellus@gmail.com")
            .Body = Contenido
            .Subject = "ARCHIVOS NO IDENTIDICADOS"

            .Priority = System.Net.Mail.MailPriority.Normal
        End With

        With smtp
            .EnableSsl = True
            .Port = 587
            .Host = "smtp.gmail.com"
            .Credentials = New Net.NetworkCredential("pruebagestinmedica@gmail.com", "carloscarlos")

        End With
        Try
            Console.WriteLine("Enviando...")
            smtp.Send(message)
            Console.WriteLine("Su mensaje de correo ha sido enviado.")
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
    End Sub
End Module
